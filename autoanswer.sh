#!/bin/bash

set timeout -1
spawn ./autoanwser.sh

expect "Your name: "
send -- "expect\n"

expect "Are you human?\r
y/n: "
send -- "n\r"

expect "What is your favorite programming language?\r
Your answer: "
send -- "Java\r"

expect eof