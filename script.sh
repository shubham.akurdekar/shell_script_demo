#!/bin/bash

#Set root password
DATABASE_PASSWORD=vsiproduct@2021
#remove existing installation
echo 'removing previous mysql server installation'
sudo service mysql stop  && sudo apt-get purge mysql* && sudo rm -rf var/lib/mysql && sudo rm -rf /var/log/mysqld.log && sudo rm -rf /etc/my.cnf
#install mysql community server
echo 'installing mysql server 8.0 community edition'
sudo apt install mysql-server
#start mysql server and grep temporary password
echo 'starting mysql server for first time'
sudo service mysql start
#systemctl enable mysqld.service 2>/dev/null
tempRootPass="`sudo grep 'temporary.*root@localhost' /var/log/mysqld.log | tail -n 1 | sed 's/.*root@localhost: //'`"
#set ne password for root user
echo 'setting up new mysql server root password'
mysql -u "root" --password="$tempRootPass" --connect-expired-password -e "alter user root@localhost identified by '${DATABASE_PASSWORD}'; flush privileges;"
#Do the Basic Hardening
mysql -u root --password="$DATABASE_PASSWORD" -e "DELETE FROM mysql.user WHERE User=''; DROP DATABASE IF EXISTS test; DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'; FLUSH PRIVILEGES;"
sudo systemctl status mysqld.service
#Perform a Sanity Check
echo "Sanity check: check if password login works for root." 
mysql -u root --password="$DATABASE_PASSWORD" -e quit
# Enable Firewall
echo "Enabling Firewall Service."
sudo firewall-cmd --permanent --add-service=mysql 2>/dev/null 
sudo firewall-cmd --reload 2>/dev/null
# Final Output
echo "MySQL server installation completed, root password: $DATABASE_PASSWORD":

#ALTER USER 'root'@'localhost' IDENTIFIED WITH caching_sha2_password by 'vsiproduct@2021';