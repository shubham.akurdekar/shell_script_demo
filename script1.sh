#!/bin/bash

#Set root password
DATABASE_PASSWORD=vsiproduct@2021
#remove existing installation
echo 'Removing previous mysql server installation'
echo '----------------------------------------------------------------------------------------------------------------------------'
sudo service mysql stop
sudo apt-get remove --purge mysql-server mysql-client mysql-common -y
sudo apt-get autoremove -y
sudo apt-get autoclean
sudo rm -rf /etc/mysql
# Delete all MySQL files on your server: 
sudo find / -iname 'mysql*' -exec rm -rf {} \;
#install mysql community server
echo 'Installing mysql server 8.0 community edition'
echo '----------------------------------------------------------------------------------------------------------------------------'
sudo apt install mysql-server
#start mysql server and grep temporary password
echo 'Starting mysql server for first time'
echo '----------------------------------------------------------------------------------------------------------------------------'
sudo service mysql start
#set ne password for root user
echo 'Setting up new mysql server root password'
echo '----------------------------------------------------------------------------------------------------------------------------'
sudo mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH caching_sha2_password by '${DATABASE_PASSWORD}';"
#Perform a Sanity Check
echo "Sanity check: check if password login works for root." 
echo '----------------------------------------------------------------------------------------------------------------------------'
mysql -u root --password="$DATABASE_PASSWORD" -e quit
# Enable Firewall
echo "Enabling Firewall Service."
echo '----------------------------------------------------------------------------------------------------------------------------'
sudo firewall-cmd --permanent --add-service=mysql 2>/dev/null 
sudo firewall-cmd --reload 2>/dev/null
# Final Output
echo "MySQL server installation completed, root password: $DATABASE_PASSWORD":
echo '----------------------------------------------------------------------------------------------------------------------------'
